# Chapter 9 - Sustaining Biodiversity: Saving Species and Ecosystem Services

## Core Case Study: Where Have All the Honeybees Gone?
Honeybees provide an essential ecosystem service in that they pollinate plants and allow them to reproduce. Honeybees are also used by farmers who rent them to polinate their crops. However, honeybee populations have been declining due to *Colony Collapse Disorder*. If we continue to rely on them, it may lead to problems in the future. We could instead rely on different wild species.

`Critical Thinking`
1. Why is having a diversity of pollinator species critical to food production?
	Having a diversity of pollinator species means that if one population of pollinators is reduced, the others can still provide the essential service.

## What role do humans play in the loss of species and ecosystem services?
`Concept 9.1: Species are becoming extinct at least 1,000 times faster than the historical rate and by the end of this century, the extinction rate is porjected to be 10,000 times higher`
*biological extinction*: A species can no longer be found anywhere on earth.
*background extinction rate*: The rate at which extinction has been happening for most of earth's history - 1 in a million estimated.
99.9% of all species have gone extinct.
*mass extinction*: The extinction of a lot of species in a short amount of time
There have been five mass extinctions, during which 50-90% of the species went extinct. They have been caused by changing environmental conditions like sea levels, temperatures, ocean acidity, and large natural disasters.
Mass extinctions also allow new species to form and fill the place of existing ones, but this takes millions of years. Even so, after each mass extinction there have been more species than before it.
Humans' spread has corresponded with an increase in extinction rates because of harmful activities like habitat destruction, ocean acidification, and global warming. Extinction can also start off a domino effect of other extinctions.
Because of these extremely high rates, the world could enter a **sixth mass extinction** caused by humans. 20-50% of known species could disappear.
This mass extinction sets off the collapse of ecosystem services which we benefit. We can try to mitigate this loss by saving keystone species and others
Not only are we making current species extinct, but we are also killing the ecosystems that make it possible for new species to emerge, like coral reefs.
This also means that organisms that reproduce and adapt quickly are likely to spread and compete with other species and make them extinct as well.

*Endangered species*: A species that could soon become extinct
*Threatened(/Vulnerable) species*: A species that could soon become endangered.
The *International Union for Conservation of Nature (ICUN)* monitors the status of the world's species and publishes a list of species that are in danger. The length has been dramatically increasing.

Species can have different characteristics that make them vulnerable to extinction, such as being K-selected, being specialized, having a narrow distribution, feeding at high trophic levels, having fixed migratory patterns, being rare, being commerically valuable, or requiring large territories.

`Checkpoint For Understanding 9.1`
1. Identify the reasons for the accelerated loss of species the earth is now experiencing.
	The earth is experiencing an accelerated loss of species because humans have been destroying the habitats in which these species live and making drastic changes to the environment and to many ecosystems. They have also been consuming resources in drastic amounts.
2. Describe the impact of massive loss of species on ecosystem sevices.
	As the number of species declines, ecosystem services can start to decline in productivity as well. For example, because of Colony Collapse Disorder, honeybee populations have been declining, and less plants are getting polinated.
3. Identify some of the characteristics of species particularily vulnerable to extinction.
	Some species that are vulnerable to extinction require a large territory, have a low reproductive rate, are comercially valuable, are specialized species, or feed at high trophic levels.

## Why should we try to sustain wild species and the ecosystem services they provide?
`Concept 9.2: We should avoid hastening the extinction of wild species because of the ecosystem and economic services they provide, because it can take millions of years for nature to recover from large-scale extinctions, and because many people believe that species have a right to exist regardless of their usefulness to us.`
Orangutans are dissapearing very quickly because their forests are being cut down to make palm oil, which is used everywhere. Some are also smuggled.
Orangutans have a low birth rate, making it hard for them to increase their numbers, and may disappear within decades.
Orangutans are considereda keystone species because they spread seeds around.
We should be concerend about making species extinct because...
	* The world's species provide vital *ecosystem services* to keep us alive and support our economies
	* Some species ocntribute to *economic services* like plant species that provide food, lumber, etc. *Bioprospectors* find plants and animals that we can learn from for medicine and other stuff. *Ecotourism* is tourism that likes ecosystems and makes a lot of money.
	* It will take a long time to replace the species we lose
	* People believe that species have a right to exist despite hteir usefulness.

Checkpoint for understanding 9.2
1. Identify four primary reasons for preserving species.
	We should preserve species because they provide ecosystem services that keep us alive and boost our economies; they provide economic services that help our economies; it will take a long time to naturally replace the species that we may lose very soon; and some people believe species have a right to exist
	BOOK: GIVE BETTER EXAMPLES

## How do humans accelerate species extinction and degradation of ecosystem services?
`Concept 9.3: The greatest threats to species and ecosystem services are loss or degradation of habitat, harmful invasive species, human population growth, pollution, climate change, and overexploitation.`
*HIPPCO* is an acronym to remember threats to exosystem services: **`H`abitat destruction, degradation, and fragmentation; `I`nvasive species; `P`opulation growth and increasing use of resources; `P`ollution; `C`limate change; and `O`verexpolitation**

The greatest threat to species is habitat loss, degradation, and fragmentation. The biggest of these is **deforestation** in the tropical rain forests. The next largest are **destruction and degradation of coastal wetlands and coral reefs**, **the plowing of grasslands for planting of crops**, and the **pollution of lakes, streams, and oceans**.

Island species are especially vulnerable to extinction because they have nowhere else to go. 63% of Hawaiian species are at risk

*Habitat fragmentation* is when habitats are divided into small areas called *habitat islands*. This can be caused by roads, urban development, crops, etc. This makes species more vulnerable to extinction and limits the ability of some to disperse and colonize. It also leads to the *edge effect*, making more edge habitat and changing the species makeup. For example, it may let more sunlight into a forest. *edge sensitivity* is how sensitive animals are to being placed in an edge habitat.

Some *non-native species* can be beneficial, like corn, wheat, and rice, which give us food and control pests. Honeybees are a non-native species that now polinate 1/3 of the crops grown in the US. 
However, some non-native species don't have predators, competitors, or other population controls. This allows them to outcompete native species, disrupt ecosystem services, and transmit new diseases. These are called *invasive species*.

**Case Study: The Kudzu Vine and Kudzu Bugs**
Some invasive species, like the *kudzu vine*, have delibaretly been introduced. Kudzu was brought from Japan to the US to control erosion, but it grew so fast that it engulfed everything and is extremely resiliant. It has spread through a lot of the southeastern US and with warmer temperatures could spread to the north.
Kudzu is edible and tolerant, and has helped Asians in the past in curing diseases and getting through famines and healing land. Kudzu can engulf and kill trees but its powder can be used to reduce alcoholism and replace trees for paper.
The *kudzu bug* is another species from Japan which eats kudzu. However, it spreads even more rapidly than the vine, and is harmful to soy crop. Pesticides can kill them, but also promote genetic resistance. They're hard to eradicate.

Some nonnative species arrive accidentally, as stowaways, some vehicles can help them spread. Florida has a lot of infasive species, like snakes who are sold as pets and then released into the wild. For example, the burmese python can feed on aligators, who are a keystone species, and can damage food webs. They reproduce rapidly and are hard to catch.
Once an invasive species becomes established in an ecosystem, it is very expensive and hard to remove them. Scientists have tried to understand habitats better to control their populations. Treaties with other countries have banned import of some species. Educating citizens can also help.

More people means more habitat degradation and shit. Pollution also threatens species. Lots of birds and fish are killed by pesticides, for example. In the 50s and 60s, large bird populations tanked because of the use of DDT. DDT remained in the environment and was taken up in the tissues of organisms---*bioaccumulation*. The chemical became more concentrated as it moved up the food chain---*biomagnification*

*Climate change* could drive a fifth-half of all known land animals/plants to extinction by the end of the century. Climate change is cuased by burning fossil fuels. The hardest hit will be the most resistant to changes in temperature, like polar bears.

Some protected species are *poached* - illegally killed - because they are valuable on the black market. VERY valuable - rhinos' horns are valuable and they have almost been driven to extinction. Elephants are being killed for their tusks, despite laws against them, because they are so valuable.
Tigers are also being poached, and combining that with habitat loss makes their numbers extremely small. India is making reserves for and protecting tigers. Another problem with tigers is that they damage farmers' crops, and the farmers kill them.
Another problem is the use of capturing wild animals for pets for collectors. Many exotic animals are killed/die in transit on their way to collectors. A big part of this is parrots. The US has passed the *Wild Bird Conservation Act* in 1992 and made it illegal to import parrots. Some exotic animals also carry diseases, and their capture can also disrupt other species.

**Case Study: A disturbing message from birds**
70% of the world's bird species are declining, and most of it is because of stuff humans do. One in eight birds is threatened with extinction due mostly to `H` of forests.
Sharp declines in bird populations have occurd among long migrating distances because of `H` of the birds breeding habitats. 40% of waterbirds are in decline because of `H` ozf wetlands.
After `H` comes `I`, and bird-eating rats have affected 28% of the world's threatened birds. Other species include snakes and mongooses, as well as cats.
`P` for population growth threatens some bird species because it causes `H` and disruption of habitats. `P` for pollution has killed birds because of toxins like insecticides, herbicides, and oil spills.
`C` for climate changes hurts birds because of heat waves and flooding.
`O` for overexploitation threatens many parrot species and other exotic birds.
This is bad because birds are *indicator species* because they live everywhere, are easy to track, and respond quickly to changing environments. This is also bad because birds perform critical economic and ecosystem services like seed dispersoal. Extinctions could put our food in jeopardy.

*Bushmeat* used to be hunted sustainably in Africa. Nowadays, it's provided as delicacy and as food for growing populations. It has driven Miss Waldron's red colobus monkey to extinction, and contributes to the population loss of many apes such as orangutans. Another problem is that bushmeat spreads diseases like ebola and HIV/AIDS. The *US Angency for International Development* is showing people how to breed other food sources.

Checkpoint for understanding 9.3
1. What does the HIPPCO acronym stand for?
	HIPPCO stands for Habitat loss, degradation, and fragmentation; Invasive species; Population growth and resource use; Pollution; Climate change; and Overexploitation.
2. Explain how HIPPCO impacts rapidly declining bird species.
	Habitat loss threatens many birds because it removes them from their natural habitats and takes away their breeding locations. Invasive species can hunt birds while having their populations grow out of control, like bird-eating rats and feral cats. Population growth threatens birds because it causes habitat loss and degradation. Pollution threatens birds because it introduces them to toxins. Overexploitation of birds threatens their populations because they are being sold as pets and killed in transit.

## How can we sustain wild species and the ecosystem services they provide?
`Concept 9.4: We canreduce species extinction and sustain ecosystem services by establishing and enforcing national environmental laws and international reaties and by creating and protecting wildlife sanctuaries`
Some governments are establishing laws, both international and national. One of the biggest is 1975's *Convention on International Trade in Endangered Species of Wild Fauna and Flora (CITES)*. This treaty, signed by 181 countries, bans the hunting, capturing, and selling of threatened or endengered species. It has a list of species that you can't do any of those things with.
CITES helps a lot, but is limited because enforcement varies from country to country. Also, member countries can excepmt themselves from protecting any species, and most illegal shit goes on in countries who haven't signed.
Another important treaty is the *Convention on Biological Diversity (CBD)*, ratified/accepted by 196 countries, but not the US. CBD legally commits participating governments to reduce the global rate of biodiversity loss and share the benefits from the use of genetic diversity.
This convention is very important because it focuses on ecosystems and not individual species. It links biodiversity protection to other issues like human rights. However, implementation has been slow.

The US enacted the *Endangered Species Act (ESA)* in **1973** and amended it several times since. It is designed to identify and protect endangered species. It creates recovery programs for the species it lists. It tries to make it so that species don't need to be on the list anymore. Under it, no projects can be done that jeopardize endangered species. Citizens may be fined if offenses are committed on their private land, making it controversial. Private landowners are given incentives to save endangered species. The ESA requires that the transport of wildlife is inspected. The ESA has been wildly successful.

In **1903**, Theodore Roosevelt established the first *US fedral wildlife refuge* at Pelican Island, Florida, to help protect the brown pelican. This bird has since been removed from the endangered list. The *National Wildlife Refuge System* is home to may refugees.
Refuges serve as sanctuaries that protect birds, such as wetlands for migratory waterfowl. They help endangered species recover.
However, harmful activities like mining and oil drilling are legally allowed in refuges.

A lot of plants are in danger of extinction. *Seed banks* are places where seeds are refrigerated and are used to preserve genetic information. The *Svalbard Global Seed Vault*, for example, contains a lot of seeds.
*Botanical gardens* contain lots of plants, and *arboretums* are land set aside for protecting plants.
We can raise some endangered species in *farms*, like floridian alligators for their meat and hides. They can also educate people.

Zoos, aquariums, game parks, and animal research centers preserve some endangered species with the goal of re-introduction.
*Egg pulling* is when eggs are collected and hatched in zoos. *Captive breeding* is when endangered species are collected for breeding in captivity, for re-introduction. 
Other techniques for increasing captive populations are *artificial insemination* (inserting semen), *embryo transfer* (transferring embryos from one mother to another), *cross fostering* (raising of young by similar species), and matching individuals for mating with DNA analysis, to increase genetic diversity.
The ultimate goal of captive breeding is to have a high enough population to reintroduce the animal into the wild. This has worked for species like the black-footed ferret, the golden lion tamarin, and the california condor. However, they have to be protected and have enough habitat. 
One problem of captive breeding is that you need a lot of animals to avoid extinction because of disease, accidents, or inbreeding.
Public aquariums help educate people about endangered species, and some carry out research, but because of lack of funding they aren't very effective.

The *Precautionary principle* says that when an activity indicates that it could harm humans or the environment, we should make sure that it doesn't happen, even if we still don't know everything about it. This is used to argue for the preservation of species and ecosystems. 

There are limited resources to help prevent extinciton, raising many questions.
	Should we focus more on species or ecosystems? How do we allocate our funds properly? How do we decide which species get the most attention? Are appealing species (e.g. pandas) more important than ecologically important ones? How do we determine which habitat to protect?

Checkpoint for understanding 9.4
1. What is CITES?
	CITES is an international treaty, signed by many countries, that prevents the hunting, capturing, and trade of endangered species in order to prevent their extinciton
2. Describe the endangered species act
	The Endangered Species Act makes a list of species that are threatened or endangered and then protects the species on that list until they don't need to be protected anymore *by restoring habitat and increasing populations*.
3. Describe the types of programs that have been established to protect and maintain endangered species.
	Some programs to maintain endangered species have set aside plots of land to protect their habitats and ecosystems. Others have bread them captively, as in zoos in and aquariums, with the goal of inflating their population so that they can be reintroduced into the wild. *Seed banks have also been created to store the genetic material of plants.*

Core Case Study Checkpoint
1. Explain why declining populations of European honeybees in the United States is a serious concern.
	Declining populations of European honeybees in the US is a serious concern because europen honeybees provide an essential ecosystem service, which is pollinating many of the crops that we use for food production, and which would be extremely expensive to replace.
