# Sustaining Terrestrial Biodiversity: Forests, Public Lands, Grasslands, Wetlands, and Cities.

## Core Case Study: Costa Rica---A Global Conservation Leader
Costa Rica was covered in Tropical Rainforests. In the 60s and 80s, a lot of it was deforested, but because of many conservation efforts by the government (like a large national park system) the government was able to drastically reduce deforestation and make Costa Rica a capital for biodiversity.

Critical Thinking
1. How does preservation of the rich biodiversity of their rainforests benefit the economy of Costa Rica?
	Costa Rica gains a lot of income from eco-tourism and doesn't have to spend as much money replacing ecosystem services that their ecosystems provide.

## How should we manage and sustain forests and public lands?
`Concept 10.1A: Forest ecosystems provide ecosystem services far greater in economic value than the value of wood and other raw materials they provide`
`Concept 10.1B: We can sustain forests by emphasizing the economic value of their ecosystem services, halting government subsidies that hasten their destruction, protecting old-growth forests, harvesting trees no faster than they are replenished, and planting trees to reestablish forests.`
An *old-growth forest* is a forest that hasn't been disturbed by himan activities or natural disasters for 200+ years. They have a lot of ecological niches
A *second-growth forest* is a stand of trees resulting from secondary ecological succession (develop after the trees have been cleared, such as by fires or by clear-cutting humans)
A *tree plantation/tree farm/commercial forest* is a managed forest containing one or two species of trees that are all the same age. Usually cut in a regular cycle with clear cutting. This can supply a lot of wood and protect everything else. Downisde = less biologically diverse/sustainable, deplete topsoil nutrients, don't provide habitat. 

Harvesting Trees: step one, build a road (many bad side effects)
There are different ways to harvest trees.
	*selective cutting* = cutting some middle-aged trees and leaving the rest intact. Can be sustainable.
	*clear cutting* = cutting every tree. Short term profitable but long term unsustainable and destroys ecosystem (soil erosion, polution, biodiversity, contributing to climate change, etc.)
	*strip cutting* = clear-cutting along a strip to allow regeneration.

*Deforestation* is the removal of large expanses of forest for agriculture, settlements, etc. Has eliminated almost half of the world's forests. By reducing forests, we release CO2 into the atmosphere and contribute to climate change. We also reduce rainfall because of less humidity, damaging topsoil (exposed to sunlight and blown away). Topsoil damage makes it harder for trees to regrow there.
Deforestation follows a pattern: Road made, biggest trees cut, land sold to people who burn the rest for grazing, land is overgrazed and sold to farmers, farmers plow it up to farm, and then the topsoil is completely destroyed.
The *fuelwood crisis* is when there's not enough trees for cooking and stuff. For example, Haiti.

Forest cover has actually spread in some countries like the US and Costa Rica and China. Old growth forests that were cut down have turned into second-growth forests.

The US has convinced people that forest fires are bad, saving lots of money and damages, but some forest fires are actually good.
A *surface fire* burns forest floor stuff and kills tiny trees. Most animals and big trees escape. This can be beneficial because it can prevent future worse fires, free nutrients, release seeds and simulate germination, and help control destructive insects.
A *crown fire* is a hot fire that burns whole trees and happens when forests haven't experience surface fires, so flammable stuff accumulates and is bad.
There are several strategies for limiting bad fires: *prescribed burns* remove flammable material, allowing surface fires to burn, protecting houses by eliminating flammable construction, and using drones to detect fires.

Forests can be grown sustainably by certifying certain material and letting consumers make the decisions. Some organizations are helping people minimize their impact under poverty. Governments are removing subsidies/tax breaks that encourage deforestation and replacing them with the opposite.
Another way is to reduce the demand for forest products. A lot of the wood used in the US is thrown away for no reason. China uses agricultural residue fibers like rice straw to mitigate this. 

**Case Study: Managing Public Lands in the United States**
The US has the most parks set aside of any country. The *National Forest System* 
