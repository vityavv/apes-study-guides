# Chapter 8 - The Human Population

## Core Case Study: Planet Earth: Population 7.5 Billion

It took 200,000 years for our population to reach 2 billion, but it took less than 50 years to reach another 2, and only 25 years to reach another 2. In 2017, there were 7.5 billion people, most of which resided in China (1.39b), India (1.35b), and the US (325m).
Some say that we can support everyone, but most say that our exponential growth is unsustainable because we use more natural resources and degrade natural capital. There is no agreed number at which the human population has been proven to be sustainable.
The reason the population is so big is because of 1) modern agriculture, 2) technology that allowed us to move everywhere, and 3) improved sanitation and medicine.
* Critical Thinking: How could a declining death rate influence a rapid rise in human population?
	The growth rate of a population is governed by its birth rate, death rate, emigration rate, and immigration rate. When the death rate falls, the population grows faster, as has happened with humans.

## How many people can the earth support?

*Demographers* - population experts
For most of human history, the population grew very slowly. In the last 200 years, it has grown rapidly, forming a J-curve of exponential growth. There are three important things going on here:
1. We are still growing exponentially, but since 1965 the rate of exponential growth has been decreasing. Now, it is 1.21%.
2. Human growth is unevenly distributed. Only 2% of thepeople born in 2017 were born into developed countries.
3. Many people are moving from rural to urban areas. In 2017, 55% of people lived in urban areas, and this is increasing. Most of these urbanites also live in lesser-developed countries, less equipped to deal with growth.

Scientists have been working to keep the population growing without the harmful natural side-effects. They disagree over how many people the earth can support forever.
As the human population grows, so does the ecological footprint, and the impact of humanity on the earth's natural capital. Research suggests humans have degraded 60% of the earth's ecosystem services.
Some say that instead of thinking about how many people the earth can support, we should try to estimate the earth's carrying capacity.
Environmental impacts include: reducing biodiversity, increasing use of NPP, increasing genetic resistance in harmful species, eliminating natural predators, introducing harmful species to natural communities, using renewable resources faster than they can be replenished, disrupting natural chemical cycling and energy flow, and relying mostly on polluting and climate-changing fossil fuels.

1. **Critical Thinking**: In your daily living, do you think you contribute direcxtly to any of these (in image) harmful environmental impacts or indirectly to any of these harmful environmental impacts? Which ones? Explain.
	I contribute indirectly to relying on polluting and climate-changing fossil fuels when I consume energy and ride in cars, trains, and busses. When I use water, I am potentially using some renewable resources faster than they can be replenished. When I eat, I am indirectly reducing biodiversity, introducing harmful species, and disrupting natural chemical cycling and energy flow, as the agriculture and farming industries do all that. Finally, I'm increasing genetic resistance in harmful species by consuming modern medecine.
2. Explain why, if the human population is growing exponentially, we have not outstripped food production when there is a finite amount of arable land.
	We have not outstripped food production because we have increased the ability of land to produce food for us with technologies such as genetic modification and more efficient farming.

**Science Focus 8.1: How Long Can the Human Population Keep Growing?**
Whether humans can keep growing indefinetly or not has been under debate since Thomas Malthus hypothesized that while humans grow exponentially, food grows linearly (slower). However, food has actually grown exponentially because of technological advances.
Some people say that we have already exceeded some of our limits. This could be because the biggest population growth is in the least developed countries, but this could also be because people in developed countries consume a lot more resources.
Other people say that technology has let us push our limits and increase the carrying capacity. They say that despite the fact that we are destroying nature, our life expectencies are still rising. They say that since we are so good at making technology, we can just keep making it and pushing away our carrying capacity.
People who say we should slow/stop population growth say that we fail to provide the basic necessities to 1b people. How are we supposed to provide for even higher populations, especially since they will grow in less developed countries? If we don't slow/stop population growth...
1. Death rates will increase, because of declining conditions. This could even lead to a population crash.
2. Resource use of renewable resources will grow, and these resources will degrade, in both developed and non-developed countries.

So far, we've been able to prevent these by advancing technology, but we are nearing several tipping points, beyond which there could be long-lasting damage. Nobody knows how close we are to these.

* Critical Thinking: Do you think there are environmental limits to human population growth? Explain.
	I think there are environmental limits to human population growth because humans are yet another species and all species have limits to their populations. Humans can try to expand their carrying capacities, but there is only so much planet left.
* If so, how close do you think we are to such limits? Explain.
	Based on recent estimates from the UN about global warming, I think that eventually global warming will set off a chain of events, possibly causing drastic population loss as close as 2030, maybe?

## What factors influence the size of human population?
Human population is controlled just like that of other species. Birth > death = growth, death == growth = static. Instead of using birth and death numbers, demographers use *crude birth rate* (births per 1,000 in a year) and *crude death rate* (deaths pre 1,000 in a year).
In a given area, migration also comes into play. *Population change* can be calculated with **(birth + immigration) - (death + emigration)**

There are two types of *fertility* rates (birth). One is the *replacement-level fertility rate*: the average number of children that couples have to bear to replace themselves. This is usually slightly higher than 2 (abt. 2.1) because some children die before being able to reproduce (esp. in less-developed countries).
Another is the *total fertility rate*, which is the average number of children born to the women of a childbearing age in a population. Between 1955 and 2017, the TRF went from 5.0 to 2.5, but to stop population growth, the TFR must remain at 2.1. Africa has a TFR of 4.6, and it is also the world's poorest continent. It is very hard to forecast TFR because there are lots of factors that come into play, like death rates and socioeconomic factors.

**Case Study: The US Population---Third Largest and Growing**
Between 1990 and 2017, the US population grew from 76m to 325m. This happened even though the TFR moved up and down. Between 1946 and 1964, the baby boom happened. At its peak (1957), the TFR was 3.7 children per woman. Since 1972, it's been at or below 2.1---1.8 in 2017.
Although the TFR has dropped, the population is still growing. Since 1820, the US has admitted almost twice as many immigrants/refugees as all other countries combined, but this number has varied over time because of politics.
Since 1965, 59m people have immigrated to the US. In 2013, China surpassed Mexico as the largest immigrator. Because of these immigrants, the country is more culturally diverse and has grown economically. 
There are also abt. 11m illegal imigrants, the status of which is controversial
In addition to population increase, there has also been a life span increase. From these comes more resource usage and larger ecological footprints.
It is predicted that b/w 2017 and 2050, the population will increase by 72m.

Many factors affect birth rate and TFR.
*Importance of children as part of the labor force*: poor couples have lots of children to help them survive and get by. One in ten children b/w 5 and 17 work to help their families survive.
*Cost of raising and educating children*: birth rates tend to be lower in developed countries because raising children is more expensive. It is estimated that the cost of raising a child in the US is $245,000
*Availability of pension systems*: Pensions reduce the need of couples to have children to support themselves once they cannot work anymore
*Urbanization*: People in urban areas tend to have access to birth control and have less children.
*Educational and employment opportunities available for women*: When women have education and employment they tend to have less children.
*Average age at marriage* (average age at which a woman has her first child): when the average age is >= 25, the birth rate tends to be lower.
*Availability of reliable birth control methods*
*Religious beliefs, traditions, and cultural norms*: some cultures oppose birth control/abortion or recommend a child of a specific gender.

Many factors affect death rates. The rapid population growth is largely due to decreasing death rates because of healthcare, medicine, sanitization, etc.
A useful indicator of peoples' health is the *life expectancy*. The life expectancy has risen by an extreme amount: 1955 - 48 to 2017 - 72. In 2017, Japan had the longest life expectency - 84 years. In the US, it rose 1900 - 47 to 2017 - 79. Research indicates that poverty is the biggest decreaser of life expectancy.
Another useful indicator is the *infant mortality*, the number of babies out of every 1000 born who die before they turn 1. A high infant mortality rate suggests *undernutrition* (not enough food), *malnutrition* (poor nutrition), and a lot of infectious disease. It also affects TFR, as low infant mortality = low TFR. Infant mortality rates have declined dramatically since 1965, but 4 million infants die of preventable causes during their first year alive. Between 1900 and 2017, the US infant mortality rate dropped from 165 to 5.8, but 44 other countries had higher rates than the US in 2017.

A population is also controlled by *immigration* (moving into) and *emigration* (moving out of) into/out of specific geographic areas. Some are driven by oppertunity, others by religion, others still by conflicts or oppression. There are also *environmental refugees* which have to leave their homes because of environmental impacts like environmental degradation, etc.

Checkpoint for understanding 8.2
1. Identify the driving force behind population growth over the last 100 years
	The driving force behind population growth has been the creation and usage of medicine, lowering death rates and infant mortality rates and making life expectancy higher.
2. Describe two important indicators of the overall health of a population.
	One is the life expectancy, which is the average life span of someone in a population. Another is infant mortality, which is the number of babies out of every 1000 that die before they reach their first birthday.

**Science Focus 8.2: Projecting Population Change**
Population estimates for 2050 range from 7.8b to 10.8b, because there are so many factors affecting birth rates and TFRs. Demographers have to determine ow reliable population estimates are, as some countries inflate population estimates. Then, they have to make assumptions about trends in fertility. If they are even slightly off, it can have massive effects.
Population projections are made by a variety of organizations like the International Institute for Applied Systems Analysis and the US Population Reference Bureau, but especially the UN.
* Critical Thinking: If you were in charge of the world and making decisions about resource use based on population projections, which of the projections in Figure 8.A would you rely on? Explain.
	I would rely on the UN medium-fertility variant because it's right in the middle so of all of the possibilities, it is the most neutral and most likely to be closer.

## How does a population's age structure affect its growth or decline?
The *age structure* of a population is the numbers or percentages of males and females in young, middle, and older age groups in that population. It can help determine whether a population will increase or decrease.
An *age-structure diagram* is created by plotting the total number of males and femalesin the *pre-reproductive* (0-14) area, *reproductive* (15-44) area, and *post-reproductive* age (45+). A country with a wide base, or many pre-reproductive people, will experience rapid population growth. Because of this *demographic momentum*, births in a country will rise a lot, even if the TFR is low. This growth is seen in less-developed countries.
The global population of 65+ers is projected to triple between 2016 and 2050. Because of this and lowering TFR, fewer working-age adults have to support a large number of seniors. This could lead to a shortage of workers.

**Case study: The American Baby Boom**
The American baby boom added 79 million people to the US population between 1946 and 1964, forming a bump in the age structure. The baby boom generation has a strong influence on US economy and politics because of its large population.
Since 2011, when the first baby boomers began turning 65, the number of americans becoming seniors has increased drastically and will stay high through 2030---this is called the *graying of America*. As the number of working adults declines, there might be political pressure from baby boomers to increase taxes to help support themselves. However, baby boomers have been beat by Millennials (1980-2005), changing power balance

The graying of the world's population is due to declining birth rates and medical advances. The UN says by 2050 the number of people >60yo will exceed the number of people under 15. As time goes on, more people experience pooulation declines. Some are experiencing <u>rapid</u> declines, which are more severe.
Japan has the highest number of seniors and the lowst number of people under 15. As its population declines, there will be fewer people working to pay taxes to support the elderly. Because Japan doesn't like immigration, this could threaten its economic situation. Houses in Japan have been abandoned because there is nobody to buy them or to pay for their demolishing. 
Rapid population decline threatens economy, leads to labor shortages, gives less revenue for the government, makes it harder for new businesses and technologies to develop, and makes it higher to fund elderly people.

Critical Thinking:
1. How might the projected age structure in 2035 affect you?
	As a teen, it will make me work harder and longer and pay more taxes to support the old and decaying masses.
2. Which two of these problems (dir. above) do you think are the most important?
	I think that lower economic growth and labor shortages are the worst because they could lead to the overworking of people and to a decline in a country, which would have to resort to leaving their elderly in the dust to survive.

Checkpoint for understanding:
1. Describe the factors that lead to a population decline.
	A population decline starts when birth rates start to decline and medical advances start allowing people to live longer. More people will reach post-reproductive age with less reproductive-aged people to replace them and to reproduce to replace themselves. The cycle continues.
2. Explain the impacts of a decreasing younger population on a country with a large elderly population.
	A decreasing younger population could lead to labor shortages, a loss of entreprenuership, and less government revenue, which make it harder for the country to grow economically. 

## How can we slow human population growth
There is controversy over whether we should slow population rowth or not. Some think that we need to slow population growth to protect the environment we use. They say we could do this many ways, the main one ofwhich is reducing poverty through economic developemtn.
Demographers say that as countries become more industrialized, their incomes rise, poverty declines, and their populations tend to grow more sloly. This is called *demographic transition*, and takes place in 5 stages
1. Preindustrial (slow because high birth AND death rate)
2. Early Transitional (population rises because death rates drop)
3. Late Transitional (Population rises fast but birth rates decline because of family planning)
4. Industrial (population growth slows because low birth AND death rates)
5. Postindustrial (Population growth levels off and then declines)

Some people think most less-developed countries will make a demographic transition soon because of newer technologies. Others say that countries could get stuck in stage 2 because of environmental degredation and resource depletion. This is happening in some African countries.

Lots of studies show that women have fewer children if they can control their fertility, earn an income, and have rights. There aren't many girls in the world in secondary education, so it is important to educate girls and give them more rihts and economic oppertunities to slow population growth.
Women do most of the housework and provide health care without getting payed. Poor women do a lot of work in many oparts of the world associated with labor and farming.
Even though they do more work than men (66%) they get only 10% of the pay and 2% of the land. Women also make up 70% of the world's poor and 66% of the world's illiterate. Poor women who cannot read have more children. Educating women would provide better health, a stabolizing population, and reduce poverty, give basic human rights, yada yada yada

*Fomily planning* programs educate couples and allow them to choose how many children to have and where to have them. Family planning can reatly reduce the number of unintended pregnancies, births, and abortions. Family planing has reduced population growth, infant mortality, and death in childbirth. Family planning also saves the government money they have to spend raising children.
However, lots of pregnancies, especially in less-developed countries, are unplanned. Lots of people don't have access to family planning services. Giving them access could reduce the global population size by 1b. Finally, because of culture, women are married as children a lot, despite the law, as they are seen as a financial burden on their parents.
Some people say family planning should expand to include educating men and developing better birth control for them.

**Case Study: Population Growth in India**
For 60 years, India has been trying to control its population, to no avail. Its first family planning service began in 1952 (400m people), but in 2017, India had 1.35b people and a TFR of 2.3, because of falling deaths. It is projected that by 2029, India will be the most populous country, and that by 2050, it will have a population of 1.7b.
India has the 4th largest economy and a growing middle class (100m). However, the country has lots of poverty, malnutrition, and bad environment, and lots of pepole live in slums. 30% of India's population is extremely poor. 300m don't have electricity.
Even though India has supported small families and family planning, the people in it are still having too many babies. Poor couples believe they need kids to work and care for them. Couples keep having children until they have a boy for cultural reasons, and less than half of the Indian people use birth control.
As India's economy and population rise, they will be pressured to provide their people and use more resources, when they have little.

**Case Study: Slowing Population Growth in China**
China is the world's most populous country (1.39b). Its population is projected to peak at 2030 (1.4b).
In the 1960s, China's population was growing fast, so the government only let people have one child. People with one child would recieve lots of benefits. The government also sponsored birth control and family planning. This started in 1978, and reduced the TFR from 3 to 1.8. Now, the Chinese population grows slower than the US population. 
Chinese culture prefers males, leading mothers to abort female fetuses, and leaving some future men without wifes. However, the government's help allowed millions to escape poverty and have a good life, making a large middle-class.
This middle class will consume more resources, making environmental degradation. Also, the Chinese population is aging fast. Because of concerns over this implication, the one-child policy was replaced with a two-child policy in 2015. Still, some people may not want to have 2 children.

Checkpoint for understanding:
1. Explain why educating and empowering women is key to slowing world population growth.
	Women that have educations and job oppertunities tend to have less children on average, and women who know more about family planning tend to have less children as well.
2. Explain why women in India tend to have larger families.
	Woemn in India tend to have larger families because their culture prefers male children, meaning women will keep having children until they have a son.

Core Case Study Checkpoint:
1. What is the world's population?
	7.5 billion
2. Identify the three most populous countries and describe the three factors that have led to rapid population growth
	The most populous countries are China, India, and the United States. Their populations have grown so high because death rates have fallen due to health care, technology allowing our territory to expand, and increasing food production.
