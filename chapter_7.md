# Chapter 7 - Species Interactions, Ecological Succession, and Population Control

## Core Case Study
Southern sea otters are an endangered species that live in kelp forests in the pacific. They feed on shellfish and other bottom-feeders. Their dense fur keeps them warm by trapping air bubbles.
They were hunted down from around 13-20 thousand to near extinction by the 1990s by fur traitors and commercial fishers that viewed them as competition. 
In 1977, the US Fish and Wildlife organization classified them as endangered, and they have been making a slow recovery ever since.
Why do we care? Some say humans should not cause the extinction of species. Some say that otters are appealing, and they generate lots of tourism.
Last but most, otters are a *keystone species*, meaning that they support lots of other species in their habitat. Scientists think that without otters, the habitat would be destroyed.

**Critical Thinking**
Are ethical concerns enough reason to protect endangered species like the sea otter? Explain.
	Ethical reasons are not enough to protect endangered species like the sea otter because, in a very hypothetical situation, if humans and the rest of the species gained absolutely nothing from otters, then humans should not harm themselves (economically) trying to protect them.

## How do species interact?
There are five basic types of interactions between species. These are *interspecific competition*, *predation*, *parasitism*, *mutualism*, and *commensailism*. Here are the first two:
	**Competition**: the most common interaction between species. It occurs when one or more species interact to use the same limited resources like light, food, water, and space. Competition between species is called interspecific competition. Competition between individuals of one species is called intraspecific competition.
	Competition occurs when ecological niches overlap. Given enough time, populations adapt to reduce competition. This can be seen in *resource partitioning*, in which species share the same resource, by doing things like using different parts of the resource, or using the resource at different times. For example, birds might prey in different parts of a tree.
	**Predation**: In predation, one species, the *predator*, eats part or all of another species, the *prey*. This strongly influences population sizes and other factors.
	In a kelp forest, sea urchins prey on kelp, and otters prey on sea urchins. Otters prevent the sea urchin population from growing out of control and destroying all of the kelp.
	**Predators** have lots of adaptations:
		*Herbivores* walk, swim, or fly to the plants they feed on - cows
		*Carnivores* usually use speed to chase and kill prey. Some have good eyesight - cheetas, eagles
		*Camouflage* allows predators to hide and ambush their prey - preying matis, arctic foxes
		*Chemical warfare* may be used by some animals to poison their prey - spiders, snakes
	**Prey** have evolved adaptions to protect themselves:
		Some move very quickly, and some have heightened senses to sense predators. They also may have protective shells (turtles), thick bark (trees), spines (porcupines), or thorns (cacti).
		Some prey also use camoflage to hide from their predators.
		Yet other prey use chemical warfare in a different way, by releasing poisonous, irritating, foul, or bad tastinc chemicals when attacked. 
		These animals show that they are poisonous, like the monarch butterflies and poisonous frogs, to teach animals not to eat them. Some other non-poisonous animals take advantage of this by *mimicing* the appearance of the poisonous ones.
		Yet other species use *behavioral strategies* to avoid being eaten, like scaring off the predators by appearing bigger or like another predator.
		Finally, some prey live in large herds and gain protection that way.
	At the individual level, prey and predators are harmed and benefitted by predation. At the population level, predation helps natural selection. Predators kill the worst prey, and so the better prey makes more offspring and over time the prey gets harder to catch. In turn, the predators evolve to be better catchers of it. This is called *coevolution*. It can also help control the populations of the predator and prey.
Here are the rest of the relationships:
	**Parasitism** occurs when one species, the *parasite*, lives in/on another, the *host*. The parasite benefits by taking from the host, while the host is weakened. However, the parasite usually does <u>not</u> kill the host.
	**Mutualism** is when two species interact and both benefit each-other. They can provide each-other with resources like food and shelter. For example, birs can ride on the backs of large animals and eat parasites---they get food, and the large animals are protected. Another example is clownfish, which live inside of stinging sea anemones. They get shelter, and protect the anemone from predators.
	**Commansalism** is when one species benefits but another is not effected . One example is epiphytes, air plants, which attatch themselves to trees and benefit from falling water. The trees are not effected.
### Science focus 7.1 
A kelp forest contains lots of giant kelp, which grows from the ocean floor to the surface waters. They grow very fast and tall, and are very strong. Kelp forests are one of the most biodiverse habitats. They also reduce shore erosion.
Se urchins prey on kelp plants, and can destroy a kelp forest by eating young ones. The southern sea otter is a keystone species that sustains kelp forests by controlling populations of sea urchins.
Kelp forests are also threatened by pollution, especially from agriculture, and are effected by algal blooms. Finally, they need cool waters to live, so they are also threatened by global warming.

**Critical Thinking**: List three ways in which we could reduce the degredation of giant kelp forest ecosystems.
1. Reduce runoff from agricultural sites
2. Slow global warming by reducing carbon emmissions
3. Preserve the populations of the keystone species, sea otters.

### Checkpoint for understanding 7.1
1. Species have three options when competing directly with another species. A species that cannot compete may adapt, migrate, or go extinct. Explain how this is demonstrated in interspecific relationships.
	* In the relationship of competition, over time, resource partitioning starts, showing how species can adapt, migrate, or go extinct when they are out-competed.
	WRONG, FROM BOOK
	* In an interspecific relationship, species can't compete forever. Therefore, one must adapt or migrate (resource partitioning is a form of adaption) or else face extinction.
2. A symbiotic relationship is defined as a close and long-term relationship between two organisms. Ientify the three types of symbiotic relationships. Explain why a predator-prey relationship is not considered symbiotic.
	* In parasitism, one organism benefits to the other's detriment
	* In commensalism, one organism benefits while the other is not affected
	* In mutualism, both organisms in the relationship benefit
	* A predator-prey relationship is not symbiotic because it is not a long-term relationship---a predator finds prey, and tries to kill it, and this does not take course over a long period of time.

## How do communities and ecosystems respond to changing environmental conditions?
The distribution of organisms changes in an area due to changing environemntal conditions. For terrestrial areas, this is called *ecological succession*. There are two main categories: *primary ecological succession* and *secondary ecological succession*
	**Primary Ecological Succession** is the establishment of communities from lifeless areas. It begins when there's no living matter (e.g. soil) in an ecosystem, like after a volcano eruption or glacier melt. Primary succession takes a very long time because it takes a long time to create fertile soil.
	The *pioneer species* are the first to colonize these life-less areas. Usually these include lichens and mosses that grow quickly and break down rocks into soil.
	Into that soil go small plants and insects that add more nutrients and make it more inviting for more and more organisms and so on.
	**Secondary Ecological Succession** is when terrestrial communities develop in places where soil already exists. This can happen when an ecosystem has been disturbed or destroyed, but soil remains. Because some soil is there, the succession process happens a lot faster
Ecological succession can enrich the biodiversity of areas, and as such provides an ecological service. They are also examples of *natural ecological restoration*. 
Ecologists have identified three factors that affect ecological succession: *facilitation*, in which one set of species makes niches for others (ex. lichen make soil out of rocks for herbs, which then crowd the lichen out), *inhibition*, where some species hinder the establishment and growth of other species (ex. pine needles make the ground too acidic for plants), and *tolerance*, in which plants in the late stages of succession ucceed because they aren't in competition with others (ex. shade-tolerant plants living in shady forests).

The final stage in ecological succession is a *climax community* which is assumed to be in balance with its environment. However, many ecologists are starting to think differently about the *balance of nature*. Even though ecological succession leads to more stable and resilient ecosystems, it cannot be viewed as inevitable progress. Rather, it demonstrates the competition between species for key resources. 

In reality, all living systems are constantly changing due to changing environmental conditions. Living systems have complex processes that interact to provide stability. There are two different aspects of this stability. *inertia/persistence* is the ability of an ecosystem to survive moderate disturbances. *resilience* is the ability of an ecosystem to be restored after a major disturbance. Some ecosystems have one of these properties but not the other. Tropial rain forests have high inertia because of their biodiversity, but low resilience because of theirshitty soil. Grasslands have shitty inertia because of low biodiversity, but they have high resilience, as lots of plant matter is stored underground.

### Checkpoint for understanding 7.2
1. Explain why a tropical rainforest has high resilience but low inertia.
	* Under an ecological stress, different species respond in different ways. Because of the biodiversity of tropical rainforests, they have a multitude of different responses, meaning the ecosystem is more likely to rebound after stress. However, since most of the nutrient and energy content of the ecosystem is stored in the plant matter and not in the soil, it would be harder for the ecosystem to rebound after destruction, giving it low inertia.

## What limits the growth of populations?
A *population* is a group of interbreeding individuals of the same species. *Population size* is the number of individuals in the population at a given time, which can change over time in response to changing environmental conditions. To figure out these sizes, scientists count the number in a small area and then extrapolate.
Populations vary in their distribution over their habitats, or *dispersion*. 
Most populations live together in *clumps/groups*, like southern sea otters. This allows them to live where resources are available and to protect themselves from predators. Some populations live in *uniform* rows, but this is less common. The second most common dispersion is *random* dispersion, as seen for example in dandelions.
There are four variables that govern changes in population size. *Birth* and *immigration* increase it, and *death* and *emigration* decrease it.
**Population change = (births + immigration) - (deaths + emigration)**
A population's *age structure* is it's distribution of individuals in different age groups and it can show the spead of population change. Age groups have three main categories: the *pre-reproductive stage*, the *reproductive stage*, and the *post-reproductive stage*. If the population has a lot of individuals in or close to the reproductive stage, it will increase. If it has lots of individuals in the post-reproductive stage, it will decline.

### Sampling Populations
*Quadrat sampling* = an area is divided into quadrats and some random quadrats are surveyed for their population size. The estimate of the total population density is then
**total number counted / (number of quadrats \* area of each quadrat)**
*Mark and recapture sampling* = some species are captured and marked, and then allowed to mix back in. A second group is captured and then this equation can be used to determine the population size:
**(number in first sample * number in second sample) / total number marked in the second sample**
\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=

Each population has a *range of tolerance* which is the range of environmental factors in which it can survive. The range which it preffers is the *optimum level/range*. Each individual has a slightly different range because of their genetics, health, and age. These differences allow for evolution, as those who can tolerate changes live to pass on their tolerance.
There are various factors that determine the population size and growth. The most important of these are called *limiting factors*.
For example, on land, rain in the desert is the limiting factor that prevents many plants from living there, limiting the animals, and so on.
In water, the limiting factor is usually temperature and sunlight, but many factors such as nutrient availability, acidity, salinity, and oxygen levels are important.
Another factor that can limit population sizes is *population density*, the number of individuals found in a given area (crowdedness). *Density-dependent factors* are those that become more important as density increases. This can include negative effects like disease (spreads easier) and positive ones like reproduction (easier to find a mate). *Density-independent factors*, meanwhile, are things liek climate change and weather patterns.

### Calculating Population Size
The *intrinsic growth rate (r)* is the maximum growth rate, when there is nothing stopping the growth. This is exponential growth, and can be calculated with the *rule of 70*: **70/r = x years** where x is the amount of years it takes for the population to double.
However, no population increases indefinetly, because of density-dependent and limiting factors. Population growth decelerates and reaches a *carrying capacity (K)*. This growth is called *logistic growth*:
**population = n + rn((K-n)/K)** where n is the initial population size.
\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=\=

Exponential growth is plotted as a J-shaped curve - a population increases at a fixed rate. Species that have growth like this reproduce a lot, early, making lots of offspring, like bacteria or insects. However, this can only happen in extremely favorable environmental conditions, and never lasts forever, as there are always limits to population growth.
*Environmental resistance* is all of the limiting factors together. These determine the area's *carrying capacity*, the maximum population it can sustain. The carrying capacity is not fixed and can change.
As a population nears the carrying capacity, the J-shaped curve turns into an S-shaped curve of *logistic growth*, growth that fluctuates around the carrying capacity. However, some populations don't make this smooth change, and instead use up their resource supplies and *overshoot* the carrying capacity. After that, the population experiences a sharp decline, called a *dieback* or a *population crash*, unless it can adapt fast.
Species with exponential growth are called *r-selected species* because they have a high growth rate. They make lots of offspring and usually make more than can survive to better their chances. These include frogs, algae, bacteria, and fish. They are also *opportunists*, who reproduce quickly during favorable conditions like more resources or new niches. Their population busts shortly after, forming a *boom-bust cycle*.
Species that reproduce later in life with fewer offspring and longer lives are *K-selected species*. These are usually mamals, who are born large and cared for for a while, sometimes even in herds. These populations live near the carrying capacity of their environemtnt. Many of these species are also vulnerable to extinction, like the southern sea otters.
Once a population drops to its *minimum viable population* point, it cannot survive in the wild. 

### Case study - Exploding white-tailed deer populations in the US
In 1900, the population of white-tailed deer in the us had been reduced to 500k animals, so in the 20s and 30s laws were passed to protect them. Nowadays, there are over 30 million white-tailed deerin the us. The suburban boom gave deer gardens and landscaping to grase on, as deer lived in the edges of forests for protection in the forest and grazing in the field. As such, suburbia is perfect for them.
Because of this population boom, deer are killing native vegitation, upsetting food webs; spreading lyme disease through ticks; and damaging vehicles and hurting/killing people that run into them on roads.
All of the methods we have for controlling deer populations are either dangerous, expensive, ineffective, or a combination of several of these. Meanwhile, deer damage suburban property. 

Different species with different reproductive strategies have different *life expectancies*, which can be shown by a *survivorship curve*, that shows the percentages of individuals surviving at different ages. There are three types of curves.
	**Type I - late loss** (K-selected species) survives well to a certain age and then has high mortality
	**Type II - constant loss** like many birds will have a constant death rate
	**Type III - early loss** (r-selected species/annual plants) survivorship is low in early life.

Humans are not exempt from population crashes. When Ireland's potatoes were destroyed, they suffered one of these crases, and 1 million people died (1845). During the 14th century, the bubonic plague spread and killed one third of the population of europe (25m ppl), spreading because of crowded cities with poor sanitation.
Scientifical advances have expanded our carrying capacity but time is running out

### Checkpoint for understanding 7.3
1. Identify several benefits of organisms living in groups. Identify several disadvantages
	* Some benefits of living in groups are having organisms help each other, having shared access to a specific resource, having protection from predators, and making it easier to find a mate. Some disadvantages are being more succeptible to disease, being more vulnerable to dying off because of a loss in resources, and having intra-species competition.

### Core Case Study Checkpoint
1. Explain how the recovery of the southern sea otter as a keystone species illustrates the importance of humans working to sustain the earth's biodiversity
	* If humans restore the southern sea otter, the sea urchin population will be controlled and giant kelp will be able to thrive, as will the many species that depend on it. Doing this would keep the earth's biodiversity at regular levels, instead of destroying the biodiversity in that ecosystem.
